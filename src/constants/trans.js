export const DEFAULT_LANGUAGE = 'cat';
export const FALLBACK_LANGUAGE = 'cat';
export const SUPPORTED_LANGUAGES = ['cat', 'es'];
