import Vue from 'vue'
import Router from 'vue-router'
import {Trans} from '../plugins/Translation'
import firebase from 'firebase';


import Authentication from '../components/Authentication'
import Board from '../components/Board'
import Home from '../components/Home'
import Ranking from '../components/Ranking'
import Profile from '../components/ProfileUser'

Vue.use(Router);

//en una variable per poder exportarla despres i guardarla en una variable
const router =  new Router({
    routes: [
        {
            path: '/:lang',
            component: {
                template: `<router-view></router-view>`
            },
            children: [
                {
                    path: '',
                    component: Authentication
                },
                {
                    path: 'board',
                    component: Board,
                    meta: {
                        authUser: true
                    }
                },
                {
                    path: 'home',
                    component: Home,
                    meta: {
                        authUser: true
                    }
                },
                {
                    path: 'ranking',
                    component: Ranking,
                    meta: {
                        authUser: true
                    }
                },
                {
                    path: 'profile',
                    component: Profile,
                    meta: {
                        authUser: true
                    }
                }

            ]
        },
        {
            // Redirect user to supported lang version.
            path: '*',
            redirect: to => {
                return Trans.getUserSupportedLang()
            }
        }
    ],
    mode: 'history',
    base: 'jocPadro',
});

export default router;

router.beforeEach((to, from, next) => {
    //retorna l'usuari si hi ha un usuari autentificat
    let user = firebase.auth().currentUser;
    //comprova si s'esta intentat accedir a una ruta que requereix authUser
    let auth = to.matched.some(record => record.meta.authUser);


    if (auth && !user ) {
        next('')
    } else if (!auth && user) {
        next('home')
    } else {
        next();
    }
});


