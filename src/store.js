import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        canMoveDice: false,
        lastQuestion: false,
        finishGame: false,
        sumCategory: false,
        answerCorrectSum: false,
        startCronoQuiz: false,
        stopCrono: false,
        languageSwitcher: '',
        showCrud: false,
        showHome: true,
        messageWinCategory: '',
        showMessageWinCategory: false,
        showQuiz: false,
        showQuiz2: false,
        showFinishTimeImg: false,
        showAdminHome: false,
        showVarGame: false,
    },
    mutations: {
        changeCanMoveDiceTrue (state) {
            state.canMoveDice = true;
        },
        changeCanMoveDiceFalse (state) {
            state.canMoveDice = false;
        },
        changeLastQuestionTrue (state) {
            state.lastQuestion = true;
        },
        changeLastQuestionFalse (state) {
            state.lastQuestion = false;
        },
        finishGameTrue (state) {
            state.finishGame = true;
        },
        finishGameFalse (state) {
            state.finishGame = false;
        },
        changeSumCategoryTrue (state) {
            state.sumCategory = true;
        },
        changeSumCategoryFalse (state) {
            state.sumCategory = false;
        },
        changeAnswerCorrectSumTrue (state) {
            state.answerCorrectSum = true;
        },
        changeAnswerCorrectSumFalse (state) {
            state.answerCorrectSum = false;
        },
        changeStartCronoQuizTrue (state) {
            state.startCronoQuiz = true;
        },
        changeStartCronoQuizFalse (state) {
            state.startCronoQuiz = false;
        },
        changeStopCronoTrue (state) {
            state.stopCrono = true;
        },
        changeStopCronoFalse (state) {
            state.stopCrono = false;
        },
        changelanguageSwitcher (state, lang) {
            state.languageSwitcher = lang;
        },
        showCrudTrue (state) {
            state.showCrud = true;
        },
        showCrudFalse (state) {
            state.showCrud = false;
        },
        showHomeTrue (state) {
            state.showHome = true;
        },
        showHomeFalse (state) {
            state.showHome = false;
        },
        newMessageWinCategory (state, message) {
            state.messageWinCategory = message;
        },
        showMessageWinCategoryTrue (state) {
            state.showMessageWinCategory = true;
        },
        showMessageWinCategoryFalse (state) {
            state.showMessageWinCategory = false;
        },
        showQuizTrue (state) {
            state.showQuiz = true;
        },
        showQuizFalse (state) {
            state.showQuiz = false;
        },
        showQuizTrue2 (state) {
            state.showQuiz2 = true;
        },
        showQuizFalse2 (state) {
            state.showQuiz2 = false;
        },
        showFinishTimeImgTrue (state) {
            state.showFinishTimeImg = true;
        },
        showFinishTimeImgFalse (state) {
            state.showFinishTimeImg = false;
        },
        showAdminHomeTrue (state) {
            state.showAdminHome = true;
        },
        showAdminHomeFalse (state) {
            state.showAdminHome = false;
        },
        showVarGameTrue (state) {
            state.showVarGame = true;
        },
        showVarGameFalse (state) {
            state.showVarGame = false;
        }
    }
});
