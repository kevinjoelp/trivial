import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Vuetify from 'vuetify'
import {i18n} from "./plugins/i18n.js"
import router from './router/index.js'

import store from './store.js'
import firebase from 'firebase'


Vue.use(Vuetify);
Vue.config.productionTip = false;



firebase.auth().onAuthStateChanged(function (user) {

    new Vue({
        el: '#app',
        router,
        i18n,
        store,
        render: (h) => h(App),
    });
});


