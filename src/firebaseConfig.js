import * as firebase from 'firebase'

let config = {
    apiKey: "AIzaSyCIbViYbAkjNFdr7DNScvOXfwAptjlXoQc",
    authDomain: "trivialdb-diba.firebaseapp.com",
    databaseURL: "https://trivialdb-diba.firebaseio.com",
    projectId: "trivialdb-diba",
    storageBucket: "trivialdb-diba.appspot.com",
    messagingSenderId: "3288654169"
};
firebase.initializeApp(config);

let db = firebase.database();

export {
    db
}
