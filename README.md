# trivial_vue-cli

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Pujar al firebase
````
npm run build
firebase init
select hosting
select dist
? What do you want to use as your public directory? dist
y
n
firebase deploy

````

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
